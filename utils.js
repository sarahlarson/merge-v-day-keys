export function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

export function randomizeParts(parts) {
    return Object.keys(parts).reduce((partsObj, key) => {
        return {
            ...partsObj,
            [key]: getRandomInt(0, parts[key].length)
        }
    }, []);
}