import '../styles/globals.css'
import Store from '../data/store'

function MyApp({ Component, pageProps }) {
  console.log(Store);
  return <Store><Component {...pageProps} /></Store>
}

export default MyApp
