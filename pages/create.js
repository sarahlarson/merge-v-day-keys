import { useState, useContext } from 'react'
import { useAppContext } from '../data/store'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Create.module.css'
import KeySegment from '../components/KeySegmentSelector'
import { randomizeParts, getRandomInt } from '../utils'
import { parts, messages } from '../data/constants'
import KeySegmentSelector from '../components/KeySegmentSelector'

export default function Create() {
    const [state, dispatch] = useAppContext();
    const { selectedParts, message } = state;
    const [currentStep, setCurrentStep] = useState('build');
    const [customMessage, setCustomMessage] = useState('');
    
    const randomize = () => {
        const newParts = randomizeParts(parts)
        dispatch({type: 'SET_SELECTED_PARTS', payload: newParts});
    }
    
    const handleTextInput = e => {
        setCustomMessage(e.target.value)
        dispatch({type: 'SET_MESSAGE', payload: e.target.value})
    }

    const handleClickPreset = message => {
        dispatch({type: 'SET_MESSAGE', payload: message})
        setCustomMessage('')
    }

  return (
    <div className={styles.container}>
      <Head>
        <title>Create a Key</title>
        <meta name="description" content="Create a key" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        {
            currentStep === 'build' && (
                <>
                    <div className={styles['key-configurator']}>
                        <KeySegmentSelector id='a' />
                        <KeySegmentSelector id='b' />
                        <KeySegmentSelector id='c' />
                    </div>
                    <button onClick={randomize}>Randomize!</button>

                    <button onClick={() => setCurrentStep('customize')}>Forge Key</button>
                </>
            )
        }
        {
            currentStep === 'customize' && (
                <> 
                    <div>
                        <div>
                            <h2>Your Key</h2>
                            {
                                JSON.stringify(selectedParts)
                            }
                        </div>
                        <div>
                            <h2>Your Message</h2>
                            <p>What will you ward off? Select one of these items below.</p>
                            <p>Selected Message: {message}</p>
                            <div className={styles.grid}>
                                {
                                    messages.map((message, idx) => <button onClick={() => handleClickPreset(message.text)} key={message.id}>{message.text}</button>)
                                }
                            </div>
                            <p>Or write your own</p>
                            <textarea value={customMessage} onChange={handleTextInput} />
                        </div>
                    </div>
                    <button onClick={() => setCurrentStep('build')}>Go Back to Create</button>
                </>
            )
        }
        </main>

      <footer className={styles.footer}>
      </footer>
    </div>
  )
}
