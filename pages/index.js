import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Link from 'next/link'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create a Key</title>
        <meta name="description" content="Create and share a key for Valentine's Day" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Experience Home
        </h1>
        <p>Lorem ipsum...</p>
        <Link href="/create">
          <a className='styles.button'>Configure your key!</a>
        </Link>
      </main>
    </div>
  )
}
