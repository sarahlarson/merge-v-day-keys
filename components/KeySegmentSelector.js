import { useContext } from 'react'
import { useAppContext } from '../data/store'
import styles from '../styles/Create.module.css'
import { parts } from '../data/constants'

export default function KeySegmentSelector({ id }) {
    const [state, dispatch] = useAppContext();
    const { selectedParts } = state;
    const currentPart = selectedParts[id];
    
    const advance = dir => {
        let newIndex = currentPart + dir
        if (newIndex > parts[id].length - 1) {
            newIndex = 0 
        } else if (newIndex < 0) {
            newIndex = parts[id].length - 1;
        }
        dispatch({type: 'SET_SELECTED_PARTS', payload: {
            ...selectedParts,
            [id]: newIndex
        }});
    }
    return (
        <div className={styles['key-segment-selector']}>
            <button onClick={() => advance(-1)}>&lt;</button>
            <div>{`${id} : ${parts[id][currentPart]}`}</div>
            <button onClick={() => advance(1)}>&gt;</button>
        </div>
    )
    
}