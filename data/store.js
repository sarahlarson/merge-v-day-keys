import React, {createContext, useReducer, useContext} from "react";
import reducer from "./reducer"
import { parts } from "./constants"
import { randomizeParts } from "../utils"

const AppContext = createContext();
const initialState = {
    selectedParts: randomizeParts(parts),
    message: '',
    error: null
};

export default function Store({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <AppContext.Provider value={[state, dispatch]}>
      {children}
    </AppContext.Provider>
  );
}

export function useAppContext() {
  return useContext(AppContext);
}