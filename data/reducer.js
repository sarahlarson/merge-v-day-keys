const reducer = (state, action) => {
    switch (action.type) {
        case 'SET_MESSAGE':
            return {
                ...state,
                message: action.payload
            };
        case 'SET_SELECTED_PARTS':
            return {
                ...state,
                selectedParts: action.payload
            };
        case 'SET_ERROR':
            return {
                ...state,
                error: action.payload
            };
        default:
            return state;
    }
};

export default reducer;