export const parts = {
    a: ['A1', 'A2', 'A3', 'A4'],
    b: ['B1', 'B2', 'B3', 'B4'],
    c: ['C1', 'C2', 'C3', 'C4'],
}

export const messages = [
    {id: 1, text: 'Ward off bad Tinder dates'}, 
    {id: 2, text:'For good health in the coming year'}, 
    {id: 3, text: 'Protection from the "Sunday Scaries"'},
    {id: 4, text: 'Lorem ipsum dolor sit amet'},
    {id: 5, text: 'Suspendisse eget orci consequat'},
    {id: 6, text: 'Aenean blandit massa dolor'},
]